import 'package:flutter/material.dart';
enum APP_THEME{LIGHT,DARK}

void main() {
  runApp(ContactProfilePage());
}

class MyAppTheme {
  static ThemeData appThemeLight(){
    return ThemeData(
      brightness: Brightness.light,
      appBarTheme: AppBarTheme(
        color: Colors.lightBlueAccent,
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.indigo.shade800,
      ),
    );
  }

  static ThemeData appThemeDark(){
    return ThemeData(
      brightness: Brightness.dark,
      appBarTheme: AppBarTheme(
        color: Colors.black12,
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.indigo.shade100,
      ),
    );
  }
}

class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {

  var currenTheme = APP_THEME.LIGHT;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: MyAppTheme.appThemeDark(),
      home: Scaffold(
        appBar: appBarMain(),
        body: bodyMain(),
      ),
    );
  }

  bodyMain(){
    return ListView(
      children: <Widget>[
        Column(
          children: <Widget>[
            Container(

              margin: const EdgeInsets.only(top: 100),
              width: double.infinity,
              //Height constraint at Container widget level
              height: 80,
              child: Text(
                "29°",textAlign: TextAlign.center,
                style: TextStyle(fontSize: 50),
              ),
            ),
            Icon(Icons.sunny,size: 60),

            Container(
              height: 68,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,

                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      "อำเภอเมืองชลบุรี",textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 30),
                     ),
                  ),
                ],
              ),
            ),
            Divider(
              color: Colors.lightBlueAccent,
            ),
            Container(
                margin: const EdgeInsets.only(top: 8, bottom: 8),
                child: Theme(
                    data: ThemeData(
                      iconTheme: IconThemeData(
                        color: Colors.lightBlue,
                      ),
                    ),
                    child: profileActionItem()
                )
            ),
            Divider(
              color: Colors.lightBlue,

            ),
            mobileToday(),
            Divider(
              color: Colors.lightBlue,
            ),
            mobileWednesday(),
            mobileThursday(),
            mobileFriday(),
            mobileSaturday(),
            mobileSunday(),
          ],

        ),
      ],
    );
  }

  Widget profileActionItem(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        buildCallButton(),
        buildTextButton(),
        buildVideoCallButton(),
        buildEmailButton(),
        buildDirectionButton(),
      ],
    );
  }

  Widget buildCallButton() {
    return Column(
      children: <Widget>[
        Icon(Icons.sunny),
        Text("28°"),
        Text("11.00"),
      ],
    );
  }

  Widget buildTextButton() {
    return Column(
      children: <Widget>[
        Icon(Icons.sunny),
        Text("29°"),
        Text("12.00"),
      ],
    );
  }

  Widget buildVideoCallButton() {
    return Column(
      children: <Widget>[
        Icon(Icons.sunny),
        Text("30°"),
        Text("13.00"),
      ],
    );
  }

  Widget buildEmailButton() {
    return Column(
      children: <Widget>[
        Icon(Icons.cloud),
        Text("30°"),
        Text("14.00"),
      ],
    );
  }

  Widget buildDirectionButton() {
    return Column(
      children: <Widget>[
        Icon(Icons.cloud),
        Text("30°"),
        Text("15.00"),
      ],
    );
  }
}

Widget mobileToday(){
  return ListTile(
    leading: Text("วันนี้"),
    title: Text("30°/23°"),
    trailing: Icon(Icons.sunny),
  );
}

Widget mobileWednesday(){
  return ListTile(
    leading: Text("วันพุธ"),
    title: Text("31°/23°"),
    trailing: Icon(Icons.cloud),
  );
}

Widget mobileThursday(){
  return ListTile(
    leading: Text("วันพฤหัสบดี"),
    title: Text("31°/23°"),
    trailing: Icon(Icons.cloud),
  );
}

Widget mobileFriday(){
  return ListTile(
    leading: Text("วันศุกร์"),
    title: Text("28°/21°"),
    trailing: Icon(Icons.cloud),
  );
}

Widget mobileSaturday(){
  return ListTile(
    leading: Text("วันเสาร์"),
    title: Text("28°/21°"),
    trailing: Icon(Icons.cloud),
  );
}

Widget mobileSunday(){
  return ListTile(
    leading: Text("วันอาทิตย์"),
    title: Text("28°/23°"),
    trailing: Icon(Icons.cloud),
  );
}



appBarMain(){
  return AppBar(
    //backgroundColor: Colors.lightBlueAccent,
    title: Text("Weather"),
  );
}



